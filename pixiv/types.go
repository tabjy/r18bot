package pixiv

import "io"

type Illustration struct {
	Id                string   `json:"id"`
	Title             string   `json:"title"`
	Caption           string   `json:"caption"`
	Date              string   `json:"date"`
	Tags              []string `json:"tags"`
	Url               string   `json:"url"`
	IllustType        string   `json:"illust_type"`
	IllustBookStyle   string   `json:"illust_book_style"`
	IllustPageCount   string   `json:"illust_page_count"`
	UserName          string   `json:"user_name"`
	ProfileImg        string   `json:"profile_img"`
	IllustContentType struct {
		Sexual     int  `json:"sexual"`
		Lo         bool `json:"lo"`
		Grotesque  bool `json:"grotesque"`
		Violent    bool `json:"violent"`
		Homosexual bool `json:"homosexual"`
		Drug       bool `json:"drug"`
		Thoughts   bool `json:"thoughts"`
		Antisocial bool `json:"antisocial"`
		Religion   bool `json:"religion"`
		Original   bool `json:"original"`
		Furry      bool `json:"furry"`
		Bl         bool `json:"bl"`
		Yuri       bool `json:"yuri"`
	} `json:"illust_content_type"`
	IllustSeries          bool   `json:"illust_series"`
	IllustID              int    `json:"illust_id"`
	Width                 int    `json:"width"`
	Height                int    `json:"height"`
	UserID                int    `json:"user_id"`
	Rank                  int    `json:"rank"`
	YesRank               int    `json:"yes_rank"`
	RatingCount           int    `json:"rating_count"`
	ViewCount             int    `json:"view_count"`
	IllustUploadTimestamp int    `json:"illust_upload_timestamp"`
	Attr                  string `json:"attr"`
	IsBookmarked          bool   `json:"is_bookmarked"`
	Bookmarkable          bool   `json:"bookmarkable"`

	Href string
	// ImageUrl         string
	ImageUrls struct {
		Px128X128 string `json:"px_128x128"`
		Px480Mw   string `json:"px_480mw"`
		Small     string `json:"small"`
		Medium    string `json:"medium"`
		Large     string `json:"large"`
	} `json:"image_urls"`

	ImageContentType string
	ImageReader      io.Reader

	Metadata struct {
		Pages []struct {
			ImageUrls struct {
				Px128X128 string `json:"px_128x128"`
				Px480Mw   string `json:"px_480mw"`
				Small     string `json:"small"`
				Medium    string `json:"medium"`
				Large     string `json:"large"`
			} `json:"image_urls"`
		} `json:"pages"`
	} `json:"metadata"`
}

type Filter struct {
	Type struct {
		Sexual     int
		Lo         bool
		Grotesque  bool
		Violent    bool
		Homosexual bool
		Drug       bool
		Thoughts   bool
		Antisocial bool
		Religion   bool
		Original   bool
		Furry      bool
		Bl         bool
		Yuri       bool
	}
}
