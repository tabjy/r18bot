package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/tabjy/r18bot/bot"
	"github.com/tabjy/yagl"
)

var options bot.Options

/*
	R18 Bot
	selects daily popular R-18 artwork from pixiv.net
	permission: 3197952
*/
func main() {
	r18bot := bot.New(options)
	if err := r18bot.Start(); err != nil {
		r18bot.Fatalf("unable to start r18bot: %v", err)
	}

	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	shuttingDown := false
	for {
		switch <-sigs {
		case syscall.SIGINT, syscall.SIGTERM:
			if !shuttingDown {
				r18bot.Info("r18bot shutdown in progress, press ctrl-c again for emergency shutdown")
				shuttingDown = true
				go func() {
					if err := r18bot.Stop(); err == nil {
						r18bot.Fatalf("unable to stop r18bot properly, emergency shutdown in sequence %v", err)
						sigs <- syscall.SIGINT
						return
					}
					os.Exit(0)
				}()
			} else {
				r18bot.Info("emergency shutdown issued")
				os.Exit(1)
			}
		}
	}
}

func init() {
	flags := struct {
		token   string
		aliases string

		logLvl  string
		logPath string
	}{}

	flag.StringVar(&flags.token, "token", "", "Discord bot authentication token")
	flag.StringVar(&flags.aliases, "aliases", "!r18bot", "command aliases as comma separated values")
	flag.StringVar(&flags.logLvl, "log-level", "info", "minimum level of log to show")
	flag.StringVar(&flags.logPath, "log-path", "", "path to log file (default to log to stdout only)")
	flag.Parse()

	// init logger
	logger := yagl.StdLogger()
	var lvl int
	switch strings.ToLower(flags.logLvl) {
	case "trace":
		lvl = yagl.LvlTrace
	case "debug":
		lvl = yagl.LvlDebug
	case "info":
		lvl = yagl.LvlInfo
	case "warn":
		lvl = yagl.LvlWarn
	case "error":
		lvl = yagl.LvlError
	case "panic":
		lvl = yagl.LvlPanic
	case "fatal":
		lvl = yagl.LvlFatal
	default:
		logger.Fatalf("unrecognized log level: %s\n", flags.logLvl)
	}

	outs := []io.Writer{os.Stderr}
	var foutPath string
	if flags.logPath != "" {
		foutPath, err := filepath.Abs(flags.logPath)
		fout, err := os.OpenFile(foutPath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			logger.Fatalf("unable to open log file: %s\n%v", flags.logPath, err)
		}
		outs = append(outs, fout)
	}

	logger = yagl.New(
		yagl.FlgDate|yagl.FlgTime|yagl.FlgShortFile,
		lvl,
		outs...,
	)

	options.Logger = logger

	// check token
	if flags.token == "" {
		logger.Fatalf("token string cannot be empty")
	}
	options.Token = flags.token

	// tokenize aliases
	options.Aliases = strings.Split(flags.aliases, ",")

	logger.Infof(`
	 _____  __  ___  ____        _
	|  __ \/_ |/ _ \|  _ \      | |  
 	| |__) || | (_) | |_) | ___ | |_ 
 	|  _  / | |> _ <|  _ < / _ \| __|
 	| | \ \ | | (_) | |_) | (_) | |_ 
 	|_|  \_\|_|\___/|____/ \___/ \__| selects daily popular R-18 artwork from pixiv.net
	staring...
	token      : %s
	aliases    : %v
	log level  : %s
	log file   : %s`, options.Token, func() string {
		if len(options.Aliases) == 0 {
			return "(no alias)"
		}
		return fmt.Sprint(options.Aliases)
	}(), strings.ToLower(flags.logLvl), func() string {
		if foutPath == "" {
			return "(no log to file)"
		}
		return foutPath
	}())
}
