package bot

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/tabjy/r18bot/pixiv"
	"github.com/tabjy/yagl"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/tabjy/r18bot/bot/command"
	"github.com/tabjy/r18bot/util"
)

type Options struct {
	Token   string
	Aliases []string
	Logger  yagl.Logger
}

type Bot struct {
	Options
	yagl.Logger

	Session *discordgo.Session

	ctx        context.Context
	cancelFunc context.CancelFunc

	filters map[string]*struct {
		Filter pixiv.Filter
		Lck    sync.RWMutex
	}
	filterLck sync.RWMutex
}

func New(opt Options) *Bot {
	s, _ := discordgo.New()
	s.Token = "Bot " + opt.Token

	b := &Bot{
		Options: opt,
		Logger:  opt.Logger,
		Session: s,

		filters: make(map[string]*struct {
			Filter pixiv.Filter
			Lck    sync.RWMutex
		}),
	}
	s.AddHandler(b.onReady)
	s.AddHandler(b.onGuildCreate)
	s.AddHandler(b.onMessageCreate)

	// patch discordgo logging function
	discordgo.Logger = func(msgL, caller int, format string, a ...interface{}) {
		_, file, line, _ := runtime.Caller(caller)
		format = fmt.Sprintf("[discordgo %s:%d %s] ", file, line, format)
		switch msgL {
		case discordgo.LogDebug:
			b.Debugf(format, a...)
		case discordgo.LogInformational:
			b.Infof(format, a...)
		case discordgo.LogWarning:
			b.Warnf(format, a...)
		case discordgo.LogError:
			b.Errorf(format, a...)
		}
	}

	return b
}

func (b *Bot) Start() error {
	b.Info("r18 bot starting...")
	b.ctx, b.cancelFunc = context.WithCancel(context.Background())
	return b.Session.Open()
}

func (b *Bot) Stop() error {
	b.Info("r18 bot stopping...")
	b.cancelFunc()
	<-b.ctx.Done()
	return b.Session.Close()
}

func (b *Bot) onReady(s *discordgo.Session, event *discordgo.Ready) {
	if err := s.UpdateStatus(0, "with Futas"); err != nil {
		b.Errorf("unable to update status: %v", err)
	}
}

func (b *Bot) onGuildCreate(s *discordgo.Session, event *discordgo.GuildCreate) {
	b.filterLck.RLock()
	if b.filters[event.Guild.ID] == nil {
		b.filterLck.RUnlock()

		b.Debugf("initializing filter for guild: %s(%s)", event.Guild.Name, event.Guild.ID)

		b.filterLck.Lock()
		b.filters[event.Guild.ID] = &struct {
			Filter pixiv.Filter
			Lck    sync.RWMutex
		}{}
		b.filterLck.Unlock()
	} else {
		b.filterLck.RUnlock()
	}

	if event.Guild.Unavailable {
		b.Debugf("guild unavailable: %s(%s)", event.Guild.Name, event.Guild.ID)
		return
	}

	joinedAt, _ := event.Guild.JoinedAt.Parse()
	if time.Since(joinedAt).Seconds() > 30 {
		return
	}

	if event.Guild.SystemChannelID == "" {
		// New member notification is off. So let's not bother them.
		return
	}

	b.Debugf("saying hello to: %s(%s)", event.Guild.Name, event.Guild.ID)

	msg := fmt.Sprintf("R18 bot at your service. %s to know more!", s.State.User.Mention())
	if _, err := s.ChannelMessageSend(event.Guild.SystemChannelID, msg); err != nil {
		b.Errorf("could not send message: %v", err)
	}
}

func (b *Bot) onMessageCreate(s *discordgo.Session, msg *discordgo.MessageCreate) {
	defer func() {
		if r := recover(); r != nil {
			b.Errorf("panic recovered: %v", r)
		}
	}()

	b.Tracef("got message on channel (%s): %s", msg.ChannelID, msg.Content)
	// Ignore all messages created by the bot itself
	if msg.Author.ID == s.State.User.ID {
		return
	}

	errMsg := fmt.Sprintf("I don't quite understand that... %s for help menu.", s.State.User.Mention())

	cmd := strings.TrimSpace(msg.Content)
	args, err := util.Tokenize(cmd)
	if err != nil {
		for _, alias := range b.Aliases {
			if strings.HasPrefix(cmd, alias) { // is syntactically invalid command
				b.Tracef("invalid command: %s", msg.Content)
				if _, err = s.ChannelMessageSend(msg.ChannelID, errMsg); err != nil {
					b.Errorf("could not send message: %v", err)
				}
				return
			}
		}
		b.Tracef("invalid command: %s", msg.Content)
		return
	}

	if args[0] != s.State.User.Mention() {
		for _, alias := range b.Aliases {
			if args[0] == alias {
				goto IsValid
			}
		}
		return // not a command string
	}

	b.Tracef("valid command: %s", msg.Content)

IsValid: // is syntactically valid command
	b.filterLck.RLock()
	env := command.Environment{
		Me:      b.Session.State.User,
		Request: msg,
		Session: b.Session,
		Filter:  b.filters[msg.GuildID],
		Logger:  b.Logger,
	}
	b.filterLck.RUnlock()

	response, err := command.Run(args[1:], &env)
	if err != nil {
		b.Error(err)
		return
	}

	if response == nil {
		b.Error("unexpected nil message")
		return
	}

	if _, err = s.ChannelMessageSendComplex(msg.ChannelID, response); err != nil {
		b.Errorf("could not send message: %v", err)
	}
}
