package command

import (
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/tabjy/r18bot/pixiv"
	"github.com/tabjy/yagl"
	"sync"
)

type Environment struct {
	Me      *discordgo.User
	Request *discordgo.MessageCreate
	Session *discordgo.Session
	Filter  *struct {
		Filter pixiv.Filter
		Lck    sync.RWMutex
	}
	Logger yagl.Logger
}

// A Command is an implementation of a dnbot Command
type Command struct {
	// Run runs the Command.
	// The args are the arguments after the Command name.
	Run func(args []string, env *Environment) (*discordgo.MessageSend, error)

	Name string

	Aliases []string

	// UsageLine is the one-line usage message.
	// The first word in the line is taken to be the Command name.
	UsageLine string

	// Short is the short description shown in the 'go help' output.
	Short string

	// Long is the long message shown in the '<@...> help <this-Command>' output.
	Long string
}

var commands []*Command

func init() {
	commands = []*Command{
		&help,
		&request,
		&set,
		&show,
	}
}

func Run(args []string, env *Environment) (*discordgo.MessageSend, error) {
	if len(args) == 0 {
		args = []string{"help"}
	}

	cmd, err := byName(args[0])
	if err != nil {
		return &discordgo.MessageSend{
			Content: err.Error(),
		}, nil
	}

	response, err := cmd.Run(args[1:], env)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func byName(name string) (*Command, error) {
	for _, cmd := range commands {
		if name == cmd.Name {
			return cmd, nil
		}

		for _, alias := range cmd.Aliases {
			if name == alias {
				return cmd, nil
			}
		}
	}

	return nil, errors.New(fmt.Sprintf("unknown command: %s", name))
}
