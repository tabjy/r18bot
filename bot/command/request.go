package command

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/tabjy/r18bot/pixiv"
	"strconv"
	"strings"
)

var request Command

func init() {
	request = Command{
		Run: func(args []string, env *Environment) (*discordgo.MessageSend, error) {
			var illust *pixiv.Illustration
			var err error
			if len(args) >= 2 && args[0] == "--id" {
				var id int
				if id, err = strconv.Atoi(args[1]); err != nil {
					return nil, err
				}

				page := 0
				if len(args) >= 4 && args[2] == "--page" {
					if page, err = strconv.Atoi(args[3]); err != nil {
						return nil, err
					}
				}

				illust, err = pixiv.ById(id, page)
			} else {
				illust, err = pixiv.RandDailyR18Illust(&env.Filter.Filter)
			}

			if err != nil {
				return nil, err
			}

			channel, err := env.Session.Channel(env.Request.ChannelID)
			if len(illust.Tags) > 0 && illust.Tags[0] == "R-18" && !channel.NSFW {
				return &discordgo.MessageSend{
					Content: "explicit artwork can only be sent to NSFW channels",
				}, nil
			}

			extension := "jpg"
			if illust.ImageContentType == "image/png" {
				extension = "png"
			}

			return &discordgo.MessageSend{
				// Content: fmt.Sprintf("**%s** by *%s*", illust.Title, illust.UserName),
				Content: illust.Href,
				Embed: &discordgo.MessageEmbed{
					URL:         illust.Href,
					Title:       illust.Title,
					Description: strings.Join(illust.Tags, ", "),
					Author: &discordgo.MessageEmbedAuthor{
						URL:  fmt.Sprintf("https://www.pixiv.net/member.php?id=%d", illust.UserID),
						Name: illust.UserName,
					},
				},
				Files: []*discordgo.File{{
					Name:        fmt.Sprintf("%d.%s", illust.IllustID, extension),
					ContentType: illust.ImageContentType,
					Reader:      illust.ImageReader,
				}},
			}, nil
		},
		Name:      "request",
		UsageLine: "request",
		Short:     "request an artwork",
		Long:      "Request a randomly drawn R-18 artwork from daily ranking. Repetition is possible.",
	}
}
