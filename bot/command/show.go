package command

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"reflect"
	"strconv"
	"strings"
)

var show Command

func init() {
	show = Command{
		Run: func(args []string, env *Environment) (*discordgo.MessageSend, error) {
			defer func() {
				if r := recover(); r != nil {
					env.Logger.Errorf("panic recovered: %x", r)
				}
			}()

			if len(args) < 1 {
				return &discordgo.MessageSend{
					Content: "at least one argument is needed",
				}, nil
			}

			exprs := strings.Split(args[0], ".")
			var val string

			switch exprs[0] {
			case "filter":
				env.Filter.Lck.Lock()
				defer env.Filter.Lck.Unlock()

				cur := reflect.ValueOf(&env.Filter.Filter).Elem()
				for i := 1; i < len(exprs); i++ {
					cur = cur.FieldByName(strings.Title(exprs[i]))
				}

				switch cur.Type().Name() {
				case "bool":
					if cur.Bool() {
						val = "true"
					} else {
						val = "false"
					}
				case "int":
					val = strconv.Itoa(int(cur.Int()))
				case "string":
					val = cur.String()
				default:
					return &discordgo.MessageSend{
						Content: fmt.Sprintf(`"%s" is not serializable`, args[0]),
					}, nil
				}

			default:
				return &discordgo.MessageSend{
					Content: fmt.Sprintf(`unknown system setting: "%s"`, exprs[0]),
				}, nil
			}

			return &discordgo.MessageSend{
				Content: fmt.Sprintf(`"%s" is set to "%s"`, args[0], val),
			}, nil
		},
		Name:      "show",
		UsageLine: "show <expr>",
		Short:     "show the value of a system setting",
		Long:      "Show the current value of an internal system setting. \n\n" + settingFields,
	}
}
